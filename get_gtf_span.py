#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/9

import argparse
from bio_helper import *


class GTFLine(object):

    def __init__(self, line):
        l_line = line_to_list(line)
        self.chr = l_line[0]
        self.start = int(l_line[3])
        self.end = int(l_line[4])


def main(input_gtf, output_bed):
    l_coordinate = []

    with open(input_gtf) as reader:
        for line in reader:
            gtf_line = GTFLine(line)
            l_coordinate.extend([gtf_line.start, gtf_line.end])
    chr = gtf_line.chr
    min_coord = min(l_coordinate)
    min_coord = correct_start_coord_for_bed(min_coord)
    max_coord = max(l_coordinate)

    with open(output_bed, 'w') as writer:
        writer.write(get_output_line([chr, str(min_coord), str(max_coord), 'span']))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_gtf')
    parser.add_argument('output_bed')
    args = parser.parse_args()

    main(args.input_gtf, args.output_bed)
