#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/7

from general_helper import *
from ordered_set import OrderedSet
from collections import OrderedDict


class TrackLine(object):

    def __init__(self, line):
        self.l_line = line_to_list(line)
        self.code = self.l_line[3]
        self.trans_id = self.l_line[4].split('|')[1]

    def get_known_trans(self):
        origin_gene, origin_trans = self.l_line[2].split('|')
        return self.trans_id, origin_trans


def classify_known_novel_trans(s_total_trans, tracking_file):
    od_known_trans = OrderedDict()
    os_novel_trans = OrderedSet()
    with open(tracking_file) as reader:
        for line in reader.readlines():
            track_line = TrackLine(line)
            if is_known_trans(track_line.code):
                t, o = track_line.get_known_trans()
                od_known_trans[t] = o

    for trans in s_total_trans:
        if trans not in od_known_trans:
            os_novel_trans.add(trans)

    return od_known_trans, os_novel_trans


def is_known_trans(code):
    if code == '=' or code == 'c':
        return True
    else:
        return False
