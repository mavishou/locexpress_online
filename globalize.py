#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/9

from general_helper import *


class GLB(object):

    def __init__(self, config, args):
        self.config = config
        self.args = args
        self.env = self.args.env
        self.species = self.args.species
        self.run_id = self.args.run_id
        self.l_sample = []
        self.__make_env_vars()
        self.__make_constrain_vars()
        self.__make_runtime_vars()

    def __make_constrain_vars(self):
        self.max_line = self.config['max_line']
        self.max_locus_span = self.config['max_locus_span']
        self.valid_chrs = self.config['valid_chrs'][self.species]

    def __make_env_vars(self):
        self.db = self.config['database_path']
        self.script_dir = self.config['script_dir']
        self.exp_table = self.config['exp_table'][self.species]
        self.gene_model = self.config['gene_model_path'][self.species]
        self.work_dir = self.config['work_dir']

    def __get_input_files(self):
        self.input_gtf = self.__get_file_in_id_dir('input_gtf')
        self.cel_type_list = self.__get_file_in_id_dir('cell_type_list')

    def __get_output_files(self):
        self.log_file = self.__get_file_in_id_dir('log_file')
        self.trans_list = self.__get_file_in_id_dir('trans_list')
        self.novel_trans_gtf = self.__get_file_in_id_dir('novel_trans_gtf')
        self.sample_list = self.__get_file_in_id_dir('sample_list')
        self.sample_info = self.__get_file_in_id_dir('sample_info')
        self.exp_result = self.__get_file_in_id_dir('exp_result')
        self.norm_exp_result = self.__get_file_in_id_dir('norm_exp_result')
        self.merged_exp_result = self.__get_file_in_id_dir('merged_exp_result')
        self.final_exp_result = self.__get_file_in_id_dir('final_exp_result')

    def __make_zip_vars(self):
        self.zip_name = 'LocExpress_%s' % self.run_id
        self.zip_for_download = '%s/%s.zip' % (self.id_dir, self.zip_name)
        self.zip_read_me_source = os.path.join(self.script_dir, self.config['zip_read_me_source'])
        self.zip_read_me_user = self.config['zip_read_me_user']

    def __make_loc_express_vars(self):
        self.loc_express_result_dir = self.__get_file_in_id_dir('loc_express_result_dir')
        self.loc_express_final_exp = self.config['loc_express_final_exp']

    def __make_runtime_vars(self):
        self.run_dir = self.config['run_dir']
        self.id_dir = '%s/%s' % (self.run_dir, self.run_id)
        self.__get_input_files()
        self.__get_output_files()
        self.__make_cuffcmp_vars()
        self.__make_loc_express_vars()
        self.__make_zip_vars()

    def __make_cuffcmp_vars(self):
        self.cuffcmp_run_dir = '%s/cuffcmp' % self.id_dir
        self.cuffcmp_tracking_file = '%s/cuffcmp.tracking' % self.cuffcmp_run_dir

    def __get_file_in_id_dir(self, f):
        return '%s/%s' % (self.id_dir, self.config[f])

    def __get_cell_type_list(self):
        self.l_cell_type = file_to_list(self.cel_type_list)

    def get_sample_list(self, db):
        self.__get_cell_type_list()
        self.l_sample = db.get_sample_list(self.l_cell_type)
