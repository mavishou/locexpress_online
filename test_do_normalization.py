#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/10

import unittest
from do_normalization import *
from collections import OrderedDict


class TestDB(unittest.TestCase):

    def test_do_normlization(self):
        od_exp_table = OrderedDict()
        od_trans1_exp = OrderedDict()
        od_trans2_exp = OrderedDict()
        od_trans1_exp['s1'] = 10
        od_trans1_exp['s2'] = 20
        od_trans2_exp['s1'] = 30
        od_trans2_exp['s2'] = 40
        od_exp_table['t1'] = od_trans1_exp
        od_exp_table['t2'] = od_trans2_exp

        norm_factor = OrderedDict()
        norm_factor['s1'] = 10
        norm_factor['s2'] = 5

        do_normalization(od_exp_table, norm_factor)
        exp_cmp = {}
        exp_cmp['t1'] = {'s1': 1, 's2': 4}
        exp_cmp['t2'] = {'s1': 3, 's2': 8}

        self.assertDictEqual(od_exp_table, exp_cmp)


if __name__ == '__main__':
    unittest.main()
