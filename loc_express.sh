#!/bin/bash

set -o nounset
set -o errexit

ID_DIR=$1
GENE_MODEL_FILE=$2
GENE_MODEL_FILE=$(echo $GENE_MODEL_FILE | sed -r 's/(.+)\.gtf/\1.sort.gtf.gz/')
WORK_DIR=$3

# env vars
BAM_DIR=$WORK_DIR/LocExpress/10_alignment/results
ST_DIR=$WORK_DIR/LocExpress/11_call_exp/results
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# input fixed vars
INPUT_GTF=novel_trans.gtf
SAMPLE_LIST=sample.list

# vars generated by this script
SPAN_BED=span.bed

get_span_from_gtf() {
    echo "Getting span bed for $INPUT_GTF..."
    python $SCRIPT_DIR/get_gtf_span.py $INPUT_GTF $SPAN_BED
}

prepare_target_dir() {
    TARGET_DIR=samples/$SAMPLE
    mkdir -p $TARGET_DIR
}

prepare_for_merge() {
    BUNDLE_FILE=$ST_DIR/$SAMPLE/bundles/$CHR.bed
    FOR_MERGE_BED=$TARGET_DIR/for_merge.bed
    bash $SCRIPT_DIR/prepare_for_merge.sh $BUNDLE_FILE $SPAN_BED $FOR_MERGE_BED
}

do_merge() {
    MERGE_BED=$TARGET_DIR/merged.bed
    mergeBed -d 51 -c 4 -o collapse -i $FOR_MERGE_BED > $MERGE_BED
}

merge_with_bundle() {
    echo "Merging bundle..."
    prepare_for_merge
    do_merge
}

get_region_to_loc_express() {
    local region_bed=$TARGET_DIR/region.bed
    echo "Getting region to run LocExpress..."
    cat $MERGE_BED | grep "span" > $region_bed

    local name
    read CHR START END name < $region_bed
    START=$(($START+1))
    REGION=$CHR:$START-$END
    echo "Region: $REGION"
}

run_cmd_for_le() {
    local cmd=$1
    local log_file=$2
    local time_cmd="time $cmd"
    echo $time_cmd
    (eval $time_cmd) > $log_file 2>&1
}

append_novel_gtf() {
    cat $INPUT_GTF >> $EXTRACT_GTF
}

extract_gtf() {
    EXTRACT_GTF=$TARGET_DIR/extract.gtf
    echo "Extracting GTF from gene model file..."
    local cmd="/home/houm/tools/htslib-1.3.1/bin/tabix $GENE_MODEL_FILE $REGION > $EXTRACT_GTF"
    run_cmd_for_le "$cmd" $TARGET_DIR/gtf.log
    append_novel_gtf
}

extract_reads() {
    BAM_FILE=$BAM_DIR/$SAMPLE/$SAMPLE.sort.bam
    EXTRACT_BAM=$TARGET_DIR/extract.bam
    echo "Extracting reads..."
    local cmd="/home/houm/tools/samtools-1.3.1/bin/samtools view -hb $BAM_FILE $REGION > $EXTRACT_BAM"
    run_cmd_for_le "$cmd" $TARGET_DIR/reads.log

}

run_stringtie() {
    ST_LOG=$TARGET_DIR/stringtie.log
    local cmd="/home/houm/tools/stringtie-1.2.3.Linux_x86_64/stringtie $EXTRACT_BAM -G $EXTRACT_GTF -o $TARGET_DIR/out.gtf -e -B -v"
    echo "Running stringtie..."
    run_cmd_for_le "$cmd" $ST_LOG

}

get_local_total_frag_len() {
    LOC_TOTAL_FRAG_LEN=$(bash $SCRIPT_DIR/cal_total_frag_len.sh $ST_LOG)
    echo "local total frag length: $LOC_TOTAL_FRAG_LEN"
}

get_global_total_frag_len() {
    GLB_TOTAL_FRAG_LEN=$(cat $ST_DIR/$SAMPLE/total_frag_len)
}

extract_raw_data() {
    RAW_DATA=$TARGET_DIR/fpkm.raw
    sed '1 d' $TARGET_DIR/t_data.ctab | cut -f 6,11,12 > $RAW_DATA

}

correct_fpkm() {
    FINAL_RESULT=$TARGET_DIR/fpkm.final
    cat $RAW_DATA | awk -v loc="$LOC_TOTAL_FRAG_LEN" -v gl="$GLB_TOTAL_FRAG_LEN" '{print $1"\t"$3*loc/gl}' > $TARGET_DIR/fpkm.final
}

do_correct() {
    get_local_total_frag_len
    get_global_total_frag_len
    extract_raw_data
    correct_fpkm
}

remove_tmp_files() {
    rm -f $TARGET_DIR/*.ctab $TARGET_DIR/*.bam $TARGET_DIR/*.gtf
}

run_loc_express_core() {
    merge_with_bundle
    get_region_to_loc_express
    extract_gtf
    extract_reads
    run_stringtie
    do_correct
    remove_tmp_files
}

run_for_each_sample() {
    CHR=$(cat $SPAN_BED | cut -f 1)
    cat $SAMPLE_LIST | while read SAMPLE; do
        echo "----------------------------"
        echo "Run for $SAMPLE..."
        prepare_target_dir
        time (run_loc_express_core > $TARGET_DIR/run.log 2>&1)
    done
}

main() {
    cd $ID_DIR
    echo "Run for $ID_DIR"
    get_span_from_gtf
    run_for_each_sample
    echo "Done!"
}

main
