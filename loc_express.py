#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/5

import argparse
import yaml
from globalize import *
from general_checker import *
from bio_helper import *
from gtf import *
from parse_cufffcmp_result import *
from db import *
from process_loc_express_result import *
from do_normalization import *
from merge_exp import *
from zipfile import ZipFile


class LocExpress(object):
    
    def __init__(self):
        self.total_trans_list = []
        self.od_known_trans = OrderedDict()
        self.os_novel_trans = OrderedSet()
        self.od_known_exp = OrderedDict()
        self.od_exp = OrderedDict()
        self.od_merged_exp = OrderedDict()

    def __parse_cuffcmp_result(self):
        logging.info('Parsing cuffcmpare result...')
        s_total_trans = get_all_trans_id_from_gtf(glb.input_gtf)
        check_trans_empty(s_total_trans)
        self.od_known_trans, self.os_novel_trans = classify_known_novel_trans(s_total_trans, glb.cuffcmp_tracking_file)
        logging.info('Done!')

    def __output_total_trans_list(self):

        def output_known_trans_list():
            for t, o in self.od_known_trans.items():
                self.total_trans_list.append(t)
                l = get_output_line([t, 'known', o])
                writer.write(l)

        def output_novel_trans_list():
            for t in self.os_novel_trans:
                self.total_trans_list.append(t)
                l = get_output_line([t, 'novel', '-'])
                writer.write(l)

        with open(glb.trans_list, 'w') as writer:
            output_known_trans_list()
            output_novel_trans_list()

    def compare_with_known_gene_model(self):
        split_line()
        logging.info('Comparing with known gene model...')
        if glb.env != 'mock':
            run_cuffcmp()
        self.__parse_cuffcmp_result()
        self.__output_total_trans_list()

    def is_any_known_trans(self):
        if len(self.od_known_trans) > 0:
            return True
        else:
            return False

    def is_any_novel_trans(self):
        if len(self.os_novel_trans) > 0:
            return True
        else:
            return False

    def get_exp_of_known_trans(self):

        def get_exp_of_origin_trans():
            origin_trans_list = self.od_known_trans.values()
            return db.get_exp_dict(origin_trans_list, glb.l_sample, glb.exp_table)

        def map_exp_of_origin_to_known_trans():
            for t, o in self.od_known_trans.items():
                if o in od_origin_exp:
                    self.od_known_exp[t] = od_origin_exp[o]
                else:
                    error_msg = 'The origin %s of %s does not exist in expression table %s' % (o, t, glb.exp_table)
                    exit_with_run_info(error_msg, 2)

        split_line()
        logging.info('Getting expression of known transcripts...')
        od_origin_exp = get_exp_of_origin_trans()
        map_exp_of_origin_to_known_trans()
        logging.info('Done!')

    def cal_exp_of_novel_trans(self):
        split_line()
        logging.info('Calculating expression for novel transcripts...')
        list_to_file(glb.l_sample, glb.sample_list)
        logging.info('Filtering GTF for novel transcripts...')
        filter_gtf_for_trans_list(glb.input_gtf,
                                  self.os_novel_trans,
                                  glb.novel_trans_gtf)
        if glb.env != 'mock':
            run_loc_express_for_novel_trans()
        logging.info('Done!')

    def combine_known_novel_exp(self):
        split_line()
        logging.info('Combining expression of known and novel transcripts...')
        od_novel_exp = combine_exp_of_each_sample(self.os_novel_trans,
                                                  glb.l_sample,
                                                  glb.loc_express_result_dir,
                                                  glb.loc_express_final_exp)

        def combine_known_exp():
            for t, od_trans_exp in self.od_known_exp.items():
                self.od_exp[t] = od_trans_exp

        def combine_novel_exp():
            for t, od_trans_exp in od_novel_exp.items():
                self.od_exp[t] = od_trans_exp

        combine_known_exp()
        combine_novel_exp()
        output_wide_exp_table(glb.l_sample,
                              self.od_exp,
                              glb.exp_result)
        logging.info('Done!')

    def normalize(self):
        split_line()
        logging.info('Normalizing expression...')
        d_norm_factor = db.get_norm_factor(glb.l_sample)
        do_normalization(self.od_exp, d_norm_factor)
        output_wide_exp_table(glb.l_sample,
                              self.od_exp,
                              glb.norm_exp_result)
        logging.info('Done!')

    def merge_exp_for_cell_type(self):
        split_line()
        logging.info('Merging expression of the same cell type...')
        od_cell_type_to_sample = db.get_cell_type_sample_map(glb.l_cell_type)
        self.od_merged_exp = merge_exp(self.od_exp, od_cell_type_to_sample)
        output_long_exp_table(self.od_merged_exp, glb.merged_exp_result)
        logging.info('Done!')

    def output_for_user(self):

        def output_sample_info():
            d_sample_to_desc = db.get_sample_desc_map(glb.l_sample)
            with open(glb.sample_info, 'w') as writer:
                writer.write(get_output_line(['sample_id', 'cell_type']))
                for s in glb.l_sample:
                    writer.write(get_output_line([s, d_sample_to_desc[s]]))

        def output_long_exp_table_with_cell_type_desc():
            d_cell_type_to_desc = db.get_cell_type_desc_map(glb.l_cell_type)
            output_long_exp_table(self.od_merged_exp, glb.final_exp_result, d_cell_type_to_desc)

        split_line()
        logging.info('Outputting user-friendly files...')
        output_sample_info()
        output_long_exp_table_with_cell_type_desc()
        logging.info('Done!')


def run_cuffcmp():
    prepare_dir(glb.cuffcmp_run_dir)
    cmd = '/home/houm/tools/cufflinks-2.2.1.Linux_x86_64/cuffcompare -T -R -r %s %s' % (glb.gene_model, glb.input_gtf)
    run_cmd(cmd, 'CuffCompare', glb.cuffcmp_run_dir)


def run_loc_express_for_novel_trans():
    cmd = 'bash %s/loc_express.sh %s %s %s' % (glb.script_dir, glb.id_dir, glb.gene_model, glb.work_dir)
    run_cmd(cmd, "LocExpress_core")


def zip_for_download():

    def get_archive_file_name(full_file_path, changed_name=''):
        if changed_name == '':
            changed_name = os.path.basename(full_file_path)
        return os.path.join(glb.zip_name, changed_name)

    def write_zip_file(full_file_path, changed_name=''):
        zip_writer.write(full_file_path, get_archive_file_name(full_file_path, changed_name))

    logging.info('Preparing zip file for user download...')
    if os.path.exists(glb.zip_for_download):
        os.remove(glb.zip_for_download)
    with ZipFile(glb.zip_for_download, 'a') as zip_writer:
        write_zip_file(glb.input_gtf)
        write_zip_file(glb.exp_result)
        write_zip_file(glb.norm_exp_result)
        write_zip_file(glb.final_exp_result)
        write_zip_file(glb.zip_read_me_source, glb.zip_read_me_user)
    logging.info('Done!')


def main():
    logging.info('Running LocExpress for %s' % glb.run_id)
    check_line_num_valid(glb.input_gtf,
                         glb.max_line)
    check_gtf(glb.input_gtf,
              glb.valid_chrs,
              glb.max_locus_span)

    loc_express = LocExpress()
    loc_express.compare_with_known_gene_model()
    glb.get_sample_list(db)

    if loc_express.is_any_known_trans():
        loc_express.get_exp_of_known_trans()
    else:
        logging.info('No known transcripts.')

    if loc_express.is_any_novel_trans():
        loc_express.cal_exp_of_novel_trans()
    else:
        logging.info('No novel transcripts.')

    loc_express.combine_known_novel_exp()
    loc_express.normalize()
    loc_express.merge_exp_for_cell_type()
    loc_express.output_for_user()
    zip_for_download()
    logging.info('All finished!')


def handle_error(error_msg, e):
    logging.critical(e)
    exit_with_run_info(error_msg, 2)


def get_my_dir():
    return os.path.dirname(os.path.realpath(sys.argv[0]))

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--run-id', required=True)
    parser.add_argument('-s', '--species', required=True)
    parser.add_argument('-e', '--env', default='production')
    args = parser.parse_args()

    try:
        script_dir = get_my_dir()
        config_file = '%s/config/global.yaml' % script_dir
        env_config_file = '%s/config/%s.yaml' % (script_dir, args.env)
        config = yaml.load(open(config_file))
        env_config = yaml.load(open(env_config_file))
        config.update(env_config)
        config['script_dir'] = script_dir
        glb = GLB(config, args)
    except Exception, e:
        error_msg = 'Initializing global variables error.'
        handle_error(error_msg, e)

    fmt = '%(asctime)s %(levelname)s: %(message)s'
    datefmt = '%y-%m-%d %H:%M:%S'
    logging.basicConfig(level='INFO', format=fmt, datefmt=datefmt,filename=glb.log_file)

    try:
        db = DB(glb.db)
    except Exception, e:
        error_msg = 'Database connection error.'
        handle_error(error_msg, e)

    try:
        main()
    except Exception, e:
        error_msg = 'Unknown error.'
        handle_error(error_msg, e)
    finally:
        db.close()

    exit_with_run_info(exit_code=0)
