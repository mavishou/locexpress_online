input.gtf: the GTF content you submitted.
exp.txt: the raw FPKM.
norm_exp.txt: the FPKM in exp.txt are normalized to get this file.
final_exp.txt: FPKMs of the same cell type are averaged. This is the FPKM displayed in the web page.