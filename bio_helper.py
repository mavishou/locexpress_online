from general_helper import *


def check_trans_empty(l_trans):
    if len(l_trans) == 0:
        error_msg = 'No transcript_id in the GTF file.'
        exit_with_run_info(error_msg)


def correct_start_coord_for_bed(n):
    c = n - 1
    return c


def output_wide_exp_table(sample_list, od_exp_table, output_file):
    def output_header():
        output_str_list_as_one_line('transcript_id', sample_list, writer)

    with open(output_file, 'w') as writer:
        output_header()
        for t, od_trans_exp in od_exp_table.items():
            l_exp = str_list(od_trans_exp.values())
            output_str_list_as_one_line(t, l_exp, writer)


def output_long_exp_table(od_exp_table, output_file, d_name_map={}):
    with open(output_file, 'w') as writer:
        writer.write(get_output_line(['transcript_id', 'cell_type', 'fpkm']))
        for t, od_trans_exp in od_exp_table.items():
            for c, value in od_trans_exp.items():
                if len(d_name_map) == 0:
                    sample_name = c
                else:
                    sample_name = d_name_map[c]
                writer.write(get_output_line([t, sample_name, str(value)]))
