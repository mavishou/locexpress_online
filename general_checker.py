#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/9

from general_helper import *


def check_line_num_valid(input_file, max_line):
    split_line()
    logging.info('Checking line numbers...')
    n = 0
    with open(input_file) as reader:
        for line in reader.readlines():
            n += 1
            if n > max_line:
                error_msg = 'The total line number of the GTF file exceeds %s.' % max_line
                exit_with_run_info(error_msg)
    logging.info('The line number is valid.')