#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/11

import unittest
from merge_exp import *


class TestMergeExp(unittest.TestCase):

    def test_merge_exp(self):
        od_exp_table = OrderedDict({'t1': OrderedDict({'s1': 1, 's2': 4, 's3': 6, 's4': 7}),
                                    't2': OrderedDict({'s1': 5, 's2': 10, 's3': 3, 's4': 6})})
        od_cell_type_to_sample = OrderedDict({'1': set(['s1', 's2']),
                                              '2': set(['s3', 's4'])})
        od_merged_exp = merge_exp(od_exp_table, od_cell_type_to_sample)
        od_cmp = OrderedDict({'t1': OrderedDict({'1': 2.5, '2': 6.5}),
                              't2': OrderedDict({'1': 7.5, '2': 4.5})})
        self.assertDictEqual(od_merged_exp, od_cmp)


if __name__ == '__main__':
    unittest.main()
