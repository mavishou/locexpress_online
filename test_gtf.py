#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/6

import unittest
from gtf import *


class TestGTF(unittest.TestCase):

    def test_is_positive_integer(self):
        l_num = ['1ne2', '1235', '-135', '0.25', '0852', '1', '0']
        l_result = [is_positive_integer(n) for n in l_num]
        self.assertListEqual(l_result, [False, True, False, False, False, True, False])

if __name__ == '__main__':
    unittest.main()