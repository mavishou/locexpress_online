#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/8

import unittest
from db import *


class TestDB(unittest.TestCase):

    def setUp(self):
        self.db = DB('locExpress.sqlite')

    def test_get_sample_list(self):
        # cell_type_list = 'test/test_db_get_sample_list.input'
        l_cell_type = ['1', '2']
        sample_list = self.db.get_sample_list(l_cell_type)
        self.assertListEqual(sample_list, ['SRR813680', 'SRR813137', 'SRR1083632', 'SRR812436'])

    def test_get_exp(self):
        trans_list = ['ENST00000000233.9', 'ENST00000000412.7']
        sample_list = ['SRR1070479', 'SRR1070884']
        d_exp = self.db.get_exp_list(trans_list, sample_list, 'human_exp')
        d_cmp = {'ENST00000000233.9': (23.037561, 33.204762),
                 'ENST00000000412.7': (7.688228, 9.564448)}
        self.assertDictEqual(d_exp, d_cmp)

    def test_norm_factor(self):
        sample_list = ['SRR1073947', 'SRR1089688']
        d_norm_factor = self.db.get_norm_factor(sample_list)
        d_cmp = {'SRR1073947': 1.34784539754787, 'SRR1089688': 0.665908165767312}
        self.assertDictEqual(d_norm_factor, d_cmp)

    def test_get_cell_type_sample_map(self):
        l_cell_type = ['1', '2']
        od_cell_type_to_sample_list = self.db.get_cell_type_sample_map(l_cell_type)
        od_cmp = OrderedDict()
        od_cmp['1'] = set(['SRR813680', 'SRR813137'])
        od_cmp['2'] = set(['SRR1083632', 'SRR812436'])
        self.assertDictEqual(od_cell_type_to_sample_list, od_cmp)

    def test_get_cell_type_desc_map(self):
        l_cell_type = ['1', '2']
        d_cell_type_to_desc = self.db.get_cell_type_desc_map(l_cell_type)
        d_cmp = {'1': 'Adipose - Subcutaneous',
                 '2': 'Brain - Cortex'}
        self.assertDictEqual(d_cell_type_to_desc, d_cmp)

    def test_get_sample_desc_map(self):
        l_sample = ['SRR813137', 'SRR1096851']
        d_sample_to_desc = self.db.get_sample_desc_map(l_sample)
        d_cmp = {'SRR813137': 'Adipose - Subcutaneous',
                 'SRR1096851': 'Brain - Hippocampus'}
        self.assertDictEqual(d_sample_to_desc, d_cmp)

    def tearDown(self):
        self.db.close()


if __name__ == '__main__':
    unittest.main()
