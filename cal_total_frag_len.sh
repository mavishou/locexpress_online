#!/bin/bash

set -o nounset
set -o errexit

ST_LOG_FILE=$1

get_reads_count() {
    READS_COUNT=$(tail $ST_LOG_FILE | grep "Total count of aligned fragments: " | cut -d ' ' -f 6)
}

get_avg_frag_len() {
    AVG_FRAG_LEN=$(tail $ST_LOG_FILE | grep "Fragment coverage length:" | cut -d ':' -f 2 | tr -d ' ')
    if [[ "$AVG_FRAG_LEN" = "nan" || "$AVG_FRAG_LEN" = "-nan" ]]; then
        AVG_FRAG_LEN=0
    fi
}

cal_total_frag_len() {
    TOTAL_FRAG_LEN=$(python -c "print $READS_COUNT*$AVG_FRAG_LEN")
}

main() {
    get_reads_count
    get_avg_frag_len
    cal_total_frag_len
    echo $TOTAL_FRAG_LEN
}

main