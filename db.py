#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/8

from general_helper import *
import sqlite3
from collections import OrderedDict


class DB(object):

    def __init__(self, db):
        self.db = db
        self.conn = sqlite3.connect(db)

    def __get_simple_list(self, sql):
        cursor = self.conn.execute(sql)
        simple_list = []
        for row in cursor:
            simple_list.append(row[0])
        return simple_list

    def __get_query_table(self, sql):
        cursor = self.conn.execute(sql)
        return cursor

    def __get_dict_result(self, sql):
        cursor = self.conn.execute(sql)
        d_result = {}
        for row in cursor:
            d_result[str(row[0])] = row[1]
        return d_result

    def get_sample_list(self, l_cell_type):
        sql = "select sample_id from sample where type_id in (%s)" % join_list(l_cell_type)
        return self.__get_simple_list(sql)

    def __get_exp_result_table(self, trans_list, sample_list, table):
        sql = "select trans_id,%s from %s where trans_id in (%s)" % \
              (join_list(sample_list),
               table,
               join_str_for_in(trans_list))
        query_result = self.__get_query_table(sql)
        return query_result

    def get_exp_list(self, trans_list, sample_list, table):
        query_result = self.__get_exp_result_table(trans_list, sample_list, table)
        od_exp = OrderedDict()
        for row in query_result:
            od_exp[row[0]] = row[1:]
        return od_exp

    def get_exp_dict(self, trans_list, sample_list, table):
        query_result = self.__get_exp_result_table(trans_list, sample_list, table)
        od_exp = OrderedDict()
        for row in query_result:
            od_line_exp = OrderedDict()
            for i in range(len(sample_list)):
                od_line_exp[sample_list[i]] = row[i+1]
            id = row[0]
            od_exp[id] = od_line_exp
        return od_exp

    def get_norm_factor(self, sample_list):
        sql = "select sample_id,factor from norm_factor where sample_id in (%s)" % join_str_for_in(sample_list)
        d_norm_factor = self.__get_dict_result(sql)
        return d_norm_factor

    def get_cell_type_sample_map(self, l_cell_type):
        sql = "select sample_id,type_id from sample where type_id in (%s)" % join_list(l_cell_type)
        result_table = self.__get_query_table(sql)
        od_cell_type_to_sample = initialize_ordered_dict_list(l_cell_type, set())
        for row in result_table:
            od_cell_type_to_sample[str(row[1])].add(row[0])
        return od_cell_type_to_sample

    def get_cell_type_desc_map(self, l_cell_type):
        sql = "select type_id,desc from cell_type where type_id in (%s)" % join_list(l_cell_type)
        d_cell_type_to_desc = self.__get_dict_result(sql)
        return d_cell_type_to_desc

    def get_sample_desc_map(self, l_sample):
        sql = "select sample_id,desc from sample left join cell_type on " \
              "sample.type_id=cell_type.type_id where sample_id in (%s)" % join_str_for_in(l_sample)
        d_sample_to_desc = self.__get_dict_result(sql)
        return d_sample_to_desc

    def close(self):
        self.conn.close()


def join_str_for_in(li):
    return "'%s'" % "','".join(li)


def join_list(li):
    str_li = [str(l) for l in li]
    return ','.join(str_li)
