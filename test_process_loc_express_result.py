#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/10

from process_loc_express_result import *

trans_list = ['MSTRG.1.1', 'MSTRG.1.3']
sample_list = file_to_list('run/novel/sample.list')

od_exp = combine_exp_of_each_sample(trans_list, sample_list, 'run/novel/samples', 'fpkm.final')
print od_exp