#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/10


def do_normalization(d_exp_table, norm_factor):
    for t, d_trans_exp in d_exp_table.items():
        for s, value in d_trans_exp.items():
            d_trans_exp[s] = value / norm_factor[s]

