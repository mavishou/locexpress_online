#!/usr/bin/env bash

set -o nounset
set -o errexit

FILE_1=$1
FILE_2=$2
OUT_FILE=$3

get_tmp_file_prefix() {
    local pid=$$
    local now=$(date +%y%m%d-%H%M%S)
    TMP_PREFIX=${pid}-$now
}

concatenate_files() {
    get_tmp_file_prefix
    C_FILE=$TMP_PREFIX.c_file.tmp
    cat $FILE_1 $FILE_2 > $C_FILE
}

sort_file() {
    cat $C_FILE | sort -k1,1V -k2,2n > $OUT_FILE
}

remove_tmp_files() {
    rm -f $C_FILE
}

main() {
    concatenate_files
    sort_file
    remove_tmp_files
}

main