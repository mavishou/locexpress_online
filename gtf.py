#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/6

from general_helper import *
import re


class GTFLine(object):

    def __init__(self, l_line):
        self.__l_line = l_line
        self.__check_colunm_num()
        self.chr = self.__l_line[0]
        self.start = self.__check_coordinate_valid(self.__l_line[3])
        self.end = self.__check_coordinate_valid(self.__l_line[4])

    def __check_colunm_num(self):
        std_colunm_num = 9
        if len(self.__l_line) == std_colunm_num:
            self.chr = self.__l_line[0]
        else:
            error_msg = 'Invalid GTF format: each line should has %s colunms.\n%s' % \
                        (std_colunm_num, '\t'.join(self.__l_line))
            exit_with_run_info(error_msg)

    def check_chr_valid(self, valid_chrs):
        if self.chr not in valid_chrs:
            error_msg = 'Invalid GTF format: invalid chromosome name: %s' % self.chr
            exit_with_run_info(error_msg)

    def __check_coordinate_valid(self, n):
        if is_positive_integer(n):
            return int(n)
        else:
            error_msg = 'Invalid GTF format: invalid coordinate: %s' % n
            exit_with_run_info(error_msg)


def check_gtf(input_gtf, valid_chrs, max_locus_span):
    split_line()
    logging.info('Cheking the GTF file...')
    s_chr = set()
    l_coordinate = []

    with open(input_gtf) as reader:
        for line in reader:
            if not is_comment_line(line):
                l_line = line_to_list(line)
                gtf_line = GTFLine(l_line)

                if len(valid_chrs) != 0:
                    gtf_line.check_chr_valid(valid_chrs)

                s_chr.add(gtf_line.chr)
                l_coordinate.extend([gtf_line.start, gtf_line.end])

    check_chr_uniq(s_chr)
    check_locus_span(l_coordinate, max_locus_span)

    logging.info('The GTF file is valid.')


def is_positive_integer(n):
    p = re.compile(r'^[1-9][0-9]*$')
    if p.match(n):
        return True
    else:
        return False


def is_comment_line(l):
    if l.startswith('#'):
        return True
    else:
        return False


def check_chr_uniq(s_chr):
    if len(s_chr) > 1:
        error_msg = 'Only transcripts on the same chromosome are allowed.'
        exit_with_run_info(error_msg)


def check_locus_span(l_coordinate, max_locus_span):
    min_coord = min(l_coordinate)
    max_coord = max(l_coordinate)
    locus_span = max_coord - min_coord + 1
    if locus_span > max_locus_span:
        error_msg = 'The submitted locus span is larger than %s bp.' % max_locus_span
        exit_with_run_info(error_msg)


def get_all_trans_id_from_gtf(input_gtf):
    s_total_trans = set()
    with open(input_gtf) as reader:
        for line in reader.readlines():
            d_anno = get_gtf_anno(line)
            if 'transcript_id' in d_anno:
                s_total_trans.add(d_anno['transcript_id'])
    return s_total_trans


def get_gtf_anno(line):
    l_line = line_to_list(line)
    annotation = l_line[8]
    d_anno = process_gtf_anno(annotation)
    return d_anno


def process_gtf_anno(anno):
    d_anno = {}
    annos = anno.split(';')
    annos = [a.strip() for a in annos if a != '' and a != ' ']
    annos = [a for a in annos if a != '']
    for a in annos:
        lA = a.split(' ')
        if len(lA) != 2:
            logging.warning('The annotation is not in the right format: %s' % a)
        if len(lA) >= 2:
            d_anno[lA[0]] = lA[1].strip('"')
    return d_anno


def filter_gtf_for_trans_list(input_gtf, trans_list, output_gtf):
    with open(input_gtf) as reader, open(output_gtf, 'w') as writer:
        for line in reader.readlines():
            d_anno = get_gtf_anno(line)
            if ('transcript_id' in d_anno) and (d_anno['transcript_id'] in trans_list):
                writer.write(line)
