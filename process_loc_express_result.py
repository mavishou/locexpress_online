#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/10

from general_helper import *


def combine_exp_of_each_sample(trans_list, sample_list, dir, result_file):
    od_exp_table = initialize_ordered_dict_table(trans_list, sample_list)
    if len(trans_list) > 0:
        for sample in sample_list:
            exp_file = '%s/%s/%s' % (dir, sample, result_file)
            with open(exp_file) as reader:
                for line in reader.readlines():
                    id, fpkm = line_to_list(line)
                    fpkm = float(fpkm)
                    if id in trans_list:
                        od_exp_table[id][sample] = fpkm
    return od_exp_table
