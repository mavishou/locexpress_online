#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
# Created by Hou Mei on 16/5/11

from general_helper import *


def merge_exp(od_exp_table, od_cell_type_to_sample):
    l_trans = od_exp_table.keys()
    l_cell_types = od_cell_type_to_sample.keys()
    od_merged_exp = initialize_ordered_dict_table(l_trans, l_cell_types)
    for t, od_merged_trans_exp in od_merged_exp.items():
        od_trans_exp = od_exp_table[t]
        for c in l_cell_types:
            l_exp = [od_trans_exp[s] for s in od_cell_type_to_sample[c]]
            od_merged_trans_exp[c] = round(mean(l_exp), 2)
    return od_merged_exp


def mean(li):
    return float(sum(li))/len(li)
